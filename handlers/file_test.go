package handlers

import (
	"fm-libs/api"

	"fm-libs/util"
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

var (
	EntityId = "3c4eaf52-baee-4b17-b726-0c88c202d24d"
	FileId   string
)

func TestFileCreate(t *testing.T) {
	fileToBeUploaded := "../globe2.png"
	file, err := os.Open(fileToBeUploaded)
	if err != nil {
		fmt.Println(err)
		return
	}

	file_bytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return
	}

	asserts := []util.Assert{
		{FileJson{
			BaseRpcReq: s,
			Name:       "me.jpg",
			Size:       13,
			Type:       "image/png",
			Entity:     "work",
			EntityId:   EntityId,
			Category:   "image",
			File:       file_bytes,
		}, api.ErrCodeSuccess},
	}

	resps, err := util.Expect("File.Create", asserts)
	if err != nil {
		t.Error(err)
		return
	}

	resp := resps[len(resps)-1]

	if FileId, err = util.GetStringField(resp.Data, "id"); err != nil {
		t.Error(err)
		return
	}
}

func TestFileGet(t *testing.T) {
	asserts := []util.Assert{
		{FileJson{BaseRpcReq: s, Id: "???"}, api.ErrCodeValidation},
		{FileJson{BaseRpcReq: s, Id: FileId}, api.ErrCodeSuccess},
	}

	_, err := util.Expect("File.Get", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestFileGetAll(t *testing.T) {
	asserts := []util.Assert{
		{FileJson{BaseRpcReq: s, PagingReq: api.PagingReq{Page: 20}}, api.ErrCodePageNotFound},
		{FileJson{BaseRpcReq: s, PagingReq: api.PagingReq{Page: 1}}, api.ErrCodeSuccess},
	}

	_, err := util.Expect("File.GetAll", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestFileDelete(t *testing.T) {
	asserts := []util.Assert{
		{FileJson{BaseRpcReq: s, Id: "???"}, api.ErrCodeValidation},
		{FileJson{BaseRpcReq: s, Id: FileId}, api.ErrCodeSuccess},
	}

	_, err := util.Expect("File.Delete", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestFileRestore(t *testing.T) {
	asserts := []util.Assert{
		{FileJson{BaseRpcReq: s, Id: "???"}, api.ErrCodeValidation},
		{FileJson{BaseRpcReq: s, Id: FileId}, api.ErrCodeSuccess},
	}

	_, err := util.Expect("File.Restore", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestFileGetByEntityCategory(t *testing.T) {
	asserts := []util.Assert{
		{FileJson{
			BaseRpcReq: s,
			PagingReq:  api.PagingReq{Page: 20},
			Entity:     "work",
			Category:   "image",
			EntityId:   EntityId,
		}, api.ErrCodePageNotFound},
		{FileJson{BaseRpcReq: s, PagingReq: api.PagingReq{Page: 1}, Entity: "vehicle", Category: ""}, api.ErrCodeValidation},
		{FileJson{BaseRpcReq: s, PagingReq: api.PagingReq{Page: 1}, Entity: "", Category: "image"}, api.ErrCodeValidation},
		{FileJson{BaseRpcReq: s, PagingReq: api.PagingReq{Page: 1}, Entity: "", Category: ""}, api.ErrCodeValidation},
		{FileJson{
			BaseRpcReq: s,
			PagingReq:  api.PagingReq{Page: 1},
			Entity:     "work",
			Category:   "image",
			EntityId:   EntityId,
		}, api.ErrCodeSuccess},
	}

	_, err := util.Expect("File.GetByEntityCategory", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}
