package models

import (
	"bytes"
	"fm-libs/dbutil"
	"fm-libs/orm"
	"fm-libs/util"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/pborman/uuid"
	"io"
	"os"
	"path"
)

var MapCategoryToString = map[string]string{
	"image": "image",
	"doc":   "doc",
}

var MapEntityToString = map[string]string{
	"vehicle": "vehicle",
	"work":    "work",
	"issue":   "issue",
}

type File struct {
	Id       string `db:"id" json:"id"`
	Name     string `db:"name" json:"name"`
	Size     int64  `db:"file_size" json:"size"`
	Type     string `db:"mime_type" json:"type"`
	Entity   string `db:"entity" json:"entity"`
	EntityId string `db:"entity_id" json:"entity_id"`
	Link     string `db:"link" json:"link"`
	Category string `db:"category" json:"category"`

	orm.Std
}

func (m File) GetName() string {
	return "file"
}

func (m File) GetTable() string {
	return "file_files"
}

func (m *File) SetId(id string) {
	m.Id = id
}

// Checks if given entity is correct by checking if it is in the map.
// Then sets it.
func (m *File) SetEntity(entity string) error {
	entity_string, ok := MapEntityToString[entity]

	if !ok {
		return fmt.Errorf("invalid entity: %s", entity)
	}
	m.Entity = entity_string
	return nil
}

// Checks if given category is correct by checking if it is in the map.
// Then sets it.
func (m *File) SetCategory(category string) error {
	category_string, ok := MapCategoryToString[category]

	if !ok {
		return fmt.Errorf("invalid category: %s", category)
	}
	m.Category = category_string
	return nil
}

// Creates a database entry and then uploads the given file
func (m *File) Create(schema, entity, cat string, file []byte) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}

	//get sql from orm cache
	sql, err := ORM.RawSql(m, "insert", args)
	if err != nil {
		return err
	}

	//generate primary key value and link
	id := uuid.New()
	m.SetId(id)
	m.Link = cf.BaseUrl + "/" + m.Id + path.Ext(m.Name)

	//rebind query
	query, bindVars, err := sqlx.Named(sql, m)
	if err != nil {
		return err
	}

	//begin transaction
	tx, err := DB.Beginx()
	if err != nil {
		return err
	}

	//run sql query in transaction and get response
	if _, err := tx.Exec(sqlx.Rebind(sqlx.DOLLAR, query), bindVars...); err != nil {
		return err
	}

	createdFile, err := os.Create(cf.UploadDir + m.Id + path.Ext(m.Name))
	if err != nil {
		tx.Rollback()
		return err
	}

	defer createdFile.Close()

	_, err = io.Copy(createdFile, bytes.NewReader(file))
	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (m *File) Get(schema, id string) error {
	args := orm.Args{
		"table":  m.GetTable(),
		"schema": schema,
	}
	return ORM.GetByPk(m, args, id)
}

func (m *File) Count(schema string) (int, error) {
	var count int
	args := orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}
	err := ORM.Get(&count, orm.ModelSqlFile(m, "count"), args)

	if err != nil {
		return 0, err
	}

	return count, nil
}

func (m *File) GetAll(schema string, sort []string, page, pageSize int) ([]File, int, error) {
	dests := []File{}

	order := dbutil.OrderMap(map[string]string{
		"id":   "id",
		"name": "name",
	}, sort, "id ASC")

	cnt, err := m.Count(schema)
	if err != nil {
		return nil, 0, err
	}

	offset, pages := util.GetPageOffset(page, pageSize, cnt)

	if offset == -1 {
		return nil, 0, ErrNotFound
	}

	if pages == -1 {
		return nil, 0, ErrPageNotFound
	}

	if pageSize < 1 {
		pageSize = 10
	}

	err = ORM.Select(&dests, orm.ModelSqlFile(m, "select"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, order, pageSize, offset)

	if err != nil {
		return nil, 0, err
	}

	return dests, pages, nil
}

func (m *File) Delete(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Delete(m, args)
}

func (m *File) Restore(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Restore(m, args)
}

func (m *File) CountByEntityCategory(schema, entity, category, entity_id string) (int, error) {
	var count int
	args := orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}
	err := ORM.Get(&count, orm.ModelSqlFile(m, "countbyentitycategory"), args, entity, category, entity_id)

	if err != nil {
		return 0, err
	}

	return count, nil
}

func (m *File) GetByEntityCategory(schema, entity, category, entity_id string, sort []string, page, pageSize int) ([]File, int, error) {
	dests := []File{}

	order := dbutil.OrderMap(map[string]string{
		"id":   "id",
		"name": "name",
	}, sort, "id ASC")

	cnt, err := m.CountByEntityCategory(schema, entity, category, entity_id)
	if err != nil {
		return nil, 0, err
	}

	offset, pages := util.GetPageOffset(page, pageSize, cnt)

	if offset == -1 {
		return nil, 0, ErrNotFound
	}

	if pages == -1 {
		return nil, 0, ErrPageNotFound
	}

	if pageSize < 1 {
		pageSize = 10
	}

	err = ORM.Select(&dests, orm.ModelSqlFile(m, "selectbyentitycategory"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, entity, category, entity_id, order, pageSize, offset)

	if err != nil {
		return nil, 0, err
	}

	return dests, pages, nil
}
